import sys
import threading
import multiprocessing

class ThreadMultiplicacao(threading.Thread):
	def __init__(self, iInicio, iFim):
		self.iInicio = iInicio
		self.iFim    = iFim
		threading.Thread.__init__(self)

	def run(self):
		for i in range(self.iInicio, self.iFim): 						
			for j in range(len(matriz02[0])):				#Para cada linha da matriz01(i), interar sobre todas as colunas da matriz02
				val = 0			
				for k in range(len(matriz02)):

					val += matriz01[i][k] * matriz02[k][j]		
						
				textoMatriz03[i][j] += str(val)
				if(k != j):
					textoMatriz03[i][j] += ":"
				else:
					textoMatriz03[i][j] += "\n"


textoMatriz03 = []
threadsExecutando = 0
MAX_THREADS = multiprocessing.cpu_count()

pathMatriz01 = sys.argv[1]    #CAPTURAR O PRIMEIRO PARAMETRO PASSADO PARA A EXECUÇÃO
pathMatriz02 = sys.argv[2]
pathMatriz03 = sys.argv[3]

txtMatriz01 = open(pathMatriz01, 'r')         #Abrir o arquivo que está em pathMatriz01 apenas para leitura(por isso o 'r')
txtMatriz02 = open(pathMatriz02, 'r')
txtMatrizDest = open(pathMatriz03, 'a')

textoMatriz01 = txtMatriz01.readlines()       #a variavel texto vai virar uma lista contendo todas as linhas do arquivo lido
										      #['3\n','1.0:2.0:3.0\n', '4.0:5.0:6.0\n', '7.0:8.0:9.0']
textoMatriz02 = txtMatriz02.readlines()       #a variavel texto vai virar um vetor contendo todas as linhas do arquivo lido

textoMatriz03 = []

txtMatriz01.close()                 
txtMatriz02.close()

matriz01 = []
matriz02 = []

for linhaMatriz01, linhaMatriz02 in zip(textoMatriz01[1:], textoMatriz02[1:]) :          #para cada linha do texto, peço para mostrar ele na tela
	matriz01.append(list(map(float, linhaMatriz01.replace("\n","").split(":"))))
	matriz02.append(list(map(float, linhaMatriz02.replace("\n","").split(":"))))

if(len(matriz01[0]) != len(matriz02)):
	print("Entrada Inválida!")
else:
	tamanhoMatriz = len(matriz01[0])

	#Inicializar a lista que irá guardar as linhas a serem escritas no TXT
	for i in range(tamanhoMatriz):
		textoMatriz03.append([''] * tamanhoMatriz)

	print("Calculando o produto de Matrizes...")

	threads = []

	for i in range(MAX_THREADS):
		iInicial = i * (tamanhoMatriz / MAX_THREADS)           #Calcular qual o range que cada thread ficará responsavel
		iFinal   = iInicial + (tamanhoMatriz / MAX_THREADS)

		t = ThreadMultiplicacao(int(iInicial), int(iFinal))
		t.start()
		threads.append(t)

	for thread in threads: 									   #Deixar as theads em modo observatório
		thread.join()

	txtGravar = str(len(matriz01))+"\n"

	txtGravar += "".join([''.join(str(aaa) for aaa in aa) for aa in textoMatriz03])
			
	txtMatrizDest.writelines(txtGravar)
	txtMatrizDest.close()
	print("Matriz Resultante salva em "+pathMatriz03)