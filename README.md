# Matriz 1000 
# Paralela 
* Processador: Core i3
* Memória: 4GB
* Versão do sistema operacional: Windows 10 x64
* Versão do compilador: 3.6.0 [MSC v.1900 64 bit (AMD64)]
* Tempo: 0:14:15.337

# Serial
* Processador: Core i3
* Memória: 4GB
* Versão do sistema operacional: Windows 10 x64
* Versão do compilador: 3.6.0 [MSC v.1900 64 bit (AMD64)]
* Tempo: 0:20:49.694

# Matriz 2000 
# Paralela 
* Processador: Core i3
* Memória: 4GB
* Versão do sistema operacional: Windows 10 x64
* Versão do compilador: 3.6.0 [MSC v.1900 64 bit (AMD64)]
* Tempo: 1:44:36.386520

# Serial
* Processador: Core i3
* Memória: 4GB
* Versão do sistema operacional: Windows 10 x64
* Versão do compilador: 3.6.0 [MSC v.1900 64 bit (AMD64)]
* Tempo: 1:45:29.291

# Matriz 3000 
# Serial 
* Processador: Core i5
* Memória: 8GB
* Versão do sistema operacional: Ubuntu 16.04 x64
* Versão do compilador: 2.7.12 [GCC 5.4.0 20160609]
* Tempo: 2:05:36.607

# Paralela 
* Processador: Core i5
* Memória: 8GB
* Versão do sistema operacional: Ubuntu 16.04 x64
* Versão do compilador: 2.7.12 [GCC 5.4.0 20160609]
* Tempo: 2:01:14.868

# Modo de Execução #
time python NOME_ALGORITMO.py CAMINHO_MATRIZ_01.TXT CAMINHO_MATRIZ_02.TXT CAMINHO_MATRIZ_RESULTANTE.TXT

Não é necessário instalar nenhuma biblioteca adicional. Basta apenas ter o python instalado